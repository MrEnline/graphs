﻿namespace Graphs
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGraphs = new System.Windows.Forms.Button();
            this.buttonChess = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonGraphs
            // 
            this.buttonGraphs.Location = new System.Drawing.Point(3, 12);
            this.buttonGraphs.Name = "buttonGraphs";
            this.buttonGraphs.Size = new System.Drawing.Size(93, 43);
            this.buttonGraphs.TabIndex = 0;
            this.buttonGraphs.Text = "Graphs";
            this.buttonGraphs.UseVisualStyleBackColor = true;
            this.buttonGraphs.Click += new System.EventHandler(this.buttonGraphs_Click);
            // 
            // buttonChess
            // 
            this.buttonChess.Location = new System.Drawing.Point(3, 58);
            this.buttonChess.Name = "buttonChess";
            this.buttonChess.Size = new System.Drawing.Size(93, 43);
            this.buttonChess.TabIndex = 1;
            this.buttonChess.Text = "Шахматная доска";
            this.buttonChess.UseVisualStyleBackColor = true;
            this.buttonChess.Click += new System.EventHandler(this.buttonChess_click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel1.Location = new System.Drawing.Point(102, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(650, 535);
            this.panel1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 44);
            this.button1.TabIndex = 3;
            this.button1.Text = "Поиск в ширину";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SearchInWidth_click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 156);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 44);
            this.button2.TabIndex = 4;
            this.button2.Text = "Поиск в глубину";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SearchInDepth_click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 664);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonChess);
            this.Controls.Add(this.buttonGraphs);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Click += new System.EventHandler(this.SearchInDepth_click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonGraphs;
        private System.Windows.Forms.Button buttonChess;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

