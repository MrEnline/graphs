﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphs
{
    public partial class Form1 : Form
    {
        Graph graphs;
        List<Label> labels;
        Graphics graphics;
        SearchInWidth siw;
        SearchInDepth sid;

        public Form1()
        {
            InitializeComponent();
            labels = new List<Label>();
            graphics = panel1.CreateGraphics();
        }

        public void CreateGraphs()
        {
            graphs = new Graph(PaintLabel);
            labels.Clear();
            panel1.Refresh();
            //добавляем вершины графа
            for (int i = 0; i < 13; i++)
                 graphs.AddTop(i);

            //добавляем ребра графа
            graphs.AddNeighbourTops(0, 1);
            graphs.AddNeighbourTops(1, 2);
            graphs.AddNeighbourTops(2, 3);
            graphs.AddNeighbourTops(3, 4);
            graphs.AddNeighbourTops(4, 1);

            graphs.AddNeighbourTops(1, 6);
            graphs.AddNeighbourTops(6, 7);
            graphs.AddNeighbourTops(7, 8);
            graphs.AddNeighbourTops(8, 9);
            graphs.AddNeighbourTops(9, 10);
            graphs.AddNeighbourTops(10, 11);
            graphs.AddNeighbourTops(11, 12);
            graphs.AddNeighbourTops(12, 5);
            graphs.AddNeighbourTops(5, 1);
        }

        int size = 8;
        public void CreateGraphsChess()
        {
            graphs = new Graph(PaintLabel);
            labels.Clear();
            panel1.Controls.Clear();
            panel1.Refresh();
            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                    graphs.AddTop(x + y * size);

            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                    for (int s = 0; s < 4; s++)             //для каждой вершины проверяется наличие 4-х смежных вершин
                    {
                        //интересный алгоритм. Запомнить!!!
                        int neighbourTop = GetNeighbourTop(x, y, s);    //получим соседнюю вершину
                        if (neighbourTop >= 0)
                            graphs.AddNeighbourTops(x + y * size, neighbourTop);    //добавим соседнюю вершину
                    }
        }

        //метод возвращает соседнюю вершину для шахматной доски
        private int GetNeighbourTop(int x, int y, int step)
        {
            switch (step)
            {
                case 0: x--; break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
            }
            if (x >= 0 && x < size && y >= 0 && y < size)
                return x + y * size;
            return -1;
        } 
        
        private void AddLabel(int numTop, int x, int y)
        {
            Label label = new Label();
            label.Text = numTop.ToString();
            label.Location = new Point(x/2, y/2);
            label.Size = new Size(30, 20);
            label.BorderStyle = BorderStyle.FixedSingle;
            label.TextAlign = ContentAlignment.MiddleCenter;
            labels.Add(label);
            panel1.Controls.Add(label);
        }

        public void ShowGraphLabels()
        {
            panel1.Controls.Clear();
            AddLabel(0, 211, 500);
            AddLabel(1, 352, 384);
            AddLabel(2, 474, 274);
            AddLabel(3, 489, 459);
            AddLabel(4, 303, 542);
            AddLabel(5, 190, 385);
            AddLabel(6, 305, 224);
            AddLabel(7, 442, 91);
            AddLabel(8, 637, 353);
            AddLabel(9, 450, 629);
            AddLabel(10, 115, 581);
            AddLabel(11, 74, 267);
            AddLabel(12, 215, 87);
            ConnectLabels();
        }

        public void ShowGraphChess()
        {
            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                    AddLabel(x + y * size, x * 80 + 40, y * 80 + 40);
            ConnectLabels();
        }

        private void buttonGraphs_Click(object sender, EventArgs e)
        {
            CreateGraphs();
            ShowGraphLabels();
        }

        private void ConnectLabels()
        {
            foreach (int a in graphs.GetAllTops())                  //получаем вершину из хэш-таблицы 
                foreach (int b in graphs.GetAllNeighbourTops(a))    //получаем смежные вершины из словаря
                    Connect(a, b);
        }

        private void Connect(int a, int b)
        {
            graphics.DrawLine(new Pen(Color.Red), CenterOf(labels[a]), CenterOf(labels[b]));
        }

        private Point CenterOf(Label label)
        {
            return new Point(label.Location.X + label.Width / 2, label.Location.Y + label.Height / 2);
        }

        private void PaintLabel(int numLabel, Graph.ColorTops colorTop)
        {
            Color color = this.BackColor;
            switch (colorTop)
            {
                case Graph.ColorTops.black: color = Color.Gray; break;
                case Graph.ColorTops.red: color = Color.Red; break;
                case Graph.ColorTops.white: color = Color.White; break;
            }
            labels[numLabel].BackColor = color;
            Refresh();
            ConnectLabels();
            Thread.Sleep(500);
        }

        //обработчики событий
        private void buttonChess_click(object sender, EventArgs e)
        {
            CreateGraphsChess();
            ShowGraphChess();
        }

        private void SearchInWidth_click(object sender, EventArgs e)
        {
            siw = new SearchInWidth(graphs);
            siw.SearchWay(0);
        }

        private void SearchInDepth_click(object sender, EventArgs e)
        {
            sid = new SearchInDepth(graphs);
            sid.SearchWay(0);
        }
    }
}
