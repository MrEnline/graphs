﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class Graph
    {
        public delegate void ToPaintTop(int numTop, Graph.ColorTops colorTop);
        public enum ColorTops
        {
            white,
            red,
            black,
            empty
        }

        Hashtable tops;                             //вершины - хранит название вершины и цвет
        Dictionary<int, List<int>> ribs;            //ребра - храниет номер вершины и смежные ребра
        ToPaintTop toPaintTop;

        public Graph(ToPaintTop toPaintTop)
        {
            tops = new Hashtable();
            ribs = new Dictionary<int, List<int>>();
            this.toPaintTop = toPaintTop;
        }

        //операции с вершинами
        public void AddTop(int numberTop, ColorTops colorTop = ColorTops.white)
        {
            tops.Add(numberTop, colorTop);
        }

        public void SetColorAllTops(ColorTops colorTops)
        {
            foreach (int index in GetAllTops())
                SetColorTop(index, colorTops);
        }

        public void SetColorTop(int numberTop, ColorTops colorTop = ColorTops.white)
        {
            if (!tops.ContainsKey(numberTop))
                return;
            if ((ColorTops)tops[numberTop] == colorTop)
                return;
            tops[numberTop] = colorTop;
            toPaintTop(numberTop, colorTop);
        }

        public ColorTops GetColorTop(int numberTop)
        {
            if (!tops.ContainsKey(numberTop))
                return ColorTops.empty;
            return (ColorTops)tops[numberTop];
        }

        public IEnumerable<int> GetAllTops()
        {
            List<int> listKeys = new List<int>();
            foreach (int top in tops.Keys)
                listKeys.Add(top);
            foreach (int top in listKeys)
                yield return top;
        }

        //Операции с ребрами
        public void AddNeighbourTops(int a, int b)
        {
            if (!tops.ContainsKey(a))
                return;
            if (!tops.ContainsKey(b))
                return;
            AddRibs(a, b);
            AddRibs(b, a);
        }

        private void AddRibs(int a, int b)
        {
            if (!ribs.ContainsKey(a))
                ribs.Add(a, new List<int>());
            if (!ribs[a].Contains(b))
                ribs[a].Add(b);
        }

        //метод получения смежных вершин для numberTop
        public IEnumerable<int> GetAllNeighbourTops(int numberTop)
        {
            if (!tops.ContainsKey(numberTop))
                yield break;
            foreach (int rib in ribs[numberTop])
                yield return rib;
        }
    }
}
