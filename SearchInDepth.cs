﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class SearchInDepth
    {
        Graph graph;
        Stack<int> stack;
        public SearchInDepth(Graph graph)
        {
            this.graph = graph;
            stack = new Stack<int>();
        }

        //белый - исходный цвет узлов
        //красный - узел добавлен в очередь
        //черный - узел извлечен из очереди и обработан
        public void SearchWay(int startPosition)
        {
            graph.SetColorAllTops(Graph.ColorTops.white);   //устанавливаем всем вершинам белый цвет
            stack.Clear();
            stack.Push(startPosition);
            graph.SetColorTop(startPosition, Graph.ColorTops.red);
            while (stack.Count > 0)
            {
                int top = stack.Pop();
                graph.SetColorTop(top, Graph.ColorTops.black);
                foreach (int item in graph.GetAllNeighbourTops(top))
                {
                    if (graph.GetColorTop(item) == Graph.ColorTops.white)   //проверяем только белые вершины, чтобы не делать лишней работы
                    {
                        stack.Push(item);
                        graph.SetColorTop(item, Graph.ColorTops.red);
                    }
                }
            }
        }
    }
}
