﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class SearchInWidth
    {
        Graph graph;
        Queue<int> queue;
        public SearchInWidth(Graph graph)
        {
            this.graph = graph;
            queue = new Queue<int>();
        }

        //белый - исходный цвет узлов
        //красный - узел добавлен в очередь
        //черный - узел извлечен из очереди и обработан
        public void SearchWay(int startPosition)
        {
            graph.SetColorAllTops(Graph.ColorTops.white);   //устанавливаем всем вершинам белый цвет
            queue.Clear();
            queue.Enqueue(startPosition);
            graph.SetColorTop(startPosition, Graph.ColorTops.red);
            while(queue.Count > 0)
            {
                int top = queue.Dequeue();
                graph.SetColorTop(top, Graph.ColorTops.black);
                foreach (int item in graph.GetAllNeighbourTops(top))
                {
                    if (graph.GetColorTop(item) == Graph.ColorTops.white)   //проверяем только белые вершины, чтобы не делать лишней работы
                    {
                        queue.Enqueue(item);
                        graph.SetColorTop(item, Graph.ColorTops.red);
                    }
                }
            }
        }
    }
}
